package vn.tinylab.controller;

import java.util.List;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import vn.tinylab.entity.Book;
import vn.tinylab.repo.BookRepository;

@RestController
@Api(tags = "Hello")
public class HelloController {
	@Autowired
	private BookRepository bookRepository;

	@GetMapping("/books")
	public List<Book> getAll() {
		return bookRepository.findAll();
	}
	
	@PostMapping("/books")
	public ResponseEntity<Book> create(@RequestBody Book book) {
		Book createdBook = bookRepository.save(book);
		return ResponseEntity.ok(createdBook);
	}
}
