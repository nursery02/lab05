package vn.tinylab.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import vn.tinylab.entity.Book;

public interface BookRepository extends JpaRepository<Book, Long> {

}
